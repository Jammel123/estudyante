const puppeteer = require("puppeteer");
let page;
let browser;
//login as Student
test("Validation for Student can Login", async () => {
  browser = await puppeteer.launch({
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128"]
  });
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });
  //username
  await page.click(".v-input #input-10");
  await page.type(".v-input #input-10", "2015-0448");
  //password
  await page.click(".v-input #password");
  await page.type(".v-input #password", "letmein123");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
  //Show dialog box
  page.on("dialog", async dialog => {
    await dialog.accept();
  });
}, 2000000);

//view student profile
test("Validation for Student can view profile", async () => {
  await page.waitForSelector(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content"
  );
  await page.click(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".sidebar-wrapper > .md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

test("Validation for Student can change password", async () => {
  await page.waitForSelector(
    ".dropdown > .md-button > .md-ripple > .md-button-content > .md-icon"
  );
  await page.click(
    ".dropdown > .md-button > .md-ripple > .md-button-content > .md-icon"
  );

  await page.waitForSelector(
    ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
  );
  await page.click(
    ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//student view grades
test("Validation for Student can view grades", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/student");
  await page.setViewport({ width: 1366, height: 768 });

  await page.waitForSelector(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content"
  );
  await page.click(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".sidebar-wrapper > .md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//student search subject to see grades
xtest("Validation for Student can search subject", async () => {
  //search
  await page.click(".v-input #input-37");
  await page.type(".v-input #input-37", "math17");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-37", "math17")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//student view prospectus
test("Validation for Student can view prospectus", async () => {
  await page.waitForSelector(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content"
  );
  await page.click(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".sidebar-wrapper > .md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//search subject in prospectus
xtest("Validation for Student can search subject in prospectus", async () => {
  await page.click(".v-input #input-37");
  await page.type(".v-input #input-37", "Computer Networks");
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-input #input-37",
      "Computer Networks"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
  await browser.close();
}, 2000000);
