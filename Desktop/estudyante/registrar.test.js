const puppeteer = require("puppeteer");
let page;
let browser;
//login as Registrar
test("Validation for Registrar can Login", async () => {
  browser = await puppeteer.launch({
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128"]
  });
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });
  //username
  await page.click(".v-input #input-10");
  await page.type(".v-input #input-10", "2015-001");
  //password
  await page.click(".v-input #password");
  await page.type(".v-input #password", "letmein123");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Registrar can changed password
test("Validation for Registrar can changed password", async () => {
  await page.waitForSelector(
    ".dropdown > .md-button > .md-ripple > .md-button-content > .md-icon"
  );
  await page.click(
    ".dropdown > .md-button > .md-ripple > .md-button-content > .md-icon"
  );

  await page.waitForSelector(
    ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
  );
  await page.click(
    ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//registrar view all student
test("Validation for registrar can view all student", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/registrar");
  await page.setViewport({ width: 1366, height: 768 });

  await page.waitForSelector(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content"
  );
  await page.click(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".sidebar-wrapper > .md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//registrar add new student
test("Validation for registrar can add new student", async () => {
  //add button
  await page.waitForSelector(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );
  await page.click(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );
  //given name
  await page.click(".v-input #input-70");
  await page.type(".v-input #input-70", "Joe");
  //lastname
  await page.click(".v-input #input-73");
  await page.type(".v-input #input-73", "bell");
  //date of birth
  await page.waitForSelector(".v-input #input-77");
  await page.click(".v-input #input-77");
  await page.waitForSelector(
    "tbody > tr > td > .v-btn--outlined > .v-btn__content"
  );
  await page.click("tbody > tr > td > .v-btn--outlined > .v-btn__content");
  //year level
  await page.click(".v-input #input-82");
  await page.type(".v-input #input-82", "2");
  //course code
  await page.click(".v-input #input-85");
  await page.type(".v-input #input-85", "bscpe");
  //save button
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Registrar Search Student
test("registrar search student", async () => {
  await page.waitForSelector(".v-input #input-37");
  await page.click(".v-input #input-37", "huck");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-37", "huck")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Registrar Update Student Information
xtest("Validation for registrar can update student information", async () => {
  //edit button
  await page.waitForSelector(
    "tbody > tr > .text-start > .mb-2 > .v-btn__content"
  );
  await page.click("tbody > tr > .text-start > .mb-2 > .v-btn__content");
  //given name
  await page.click(".v-input #input-72");
  await page.type(".v-input #input-72", "Huckie");
  //lastname
  await page.click(".v-input #input-75");
  await page.type(".v-input #input-75", "Donkey");
  //save button
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//registrar view subject enrolled
test("Validation for registrar can view enrolled subject", async () => {
  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content > p"
  );
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content > p"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Registrar enroll student a subject
xtest("Validation for registrar can enroll student a subject", async () => {
  await page.waitForSelector(
    ".v-application--wrap > .v-data-table > .v-sheet > .v-toolbar__content > .mb-2"
  );
  await page.click(
    ".v-application--wrap > .v-data-table > .v-sheet > .v-toolbar__content > .mb-2"
  );
  //student id
  await page.click(".v-input #input-58");
  await page.type(".v-input #input-58", "2019-0114");
  //Subject code
  await page.click(".v-input #input-65");
  await page.type(".v-input #input-65", "PE1");
  //section
  await page.click(".v-input #input-65");
  await page.type(".v-input #input-65", "A");

  //Save Button
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Registrar Search Student by ID
xtest("Validation for registrar can Search student by ID", async () => {
  //Search
  await page.click(".v-input #input-37");
  await page.type(".v-input #input-37", "2015-0001");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-37", "2015-0001")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Encode Grades
//Registrar view all student grades
test("Validation for registrar can view all student Grades", async () => {
  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content > p"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content > p"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Search Student
xtest("Validation for registrar can can Search Student", async () => {
  await page.click(".v-input #input-37");
  await page.type(".v-input #input-37", "2015-0001");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-37", "2015-0001")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Encode Grades
xtest("Validation for registrar can encode grades to student", async () => {
  //encode button
  await page.waitForSelector(
    "table > tbody > tr:nth-child(1) > .text-start > .mb-2"
  );
  await page.click("table > tbody > tr:nth-child(1) > .text-start > .mb-2");

  //grade text box
  await page.click(".v-input #input-65");
  await page.type(".v-input #input-65", "95");

  //save button
  await page.waitForSelector(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );
  await page.click(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
  await browser.close();
}, 2000000);
