const puppeteer = require("puppeteer");
let page;
let browser;
//login as admin
test("Validation for Admin can Login", async () => {
  browser = await puppeteer.launch({
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128"]
  });
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });
  //username
  await page.click(".v-input #input-10");
  await page.type(".v-input #input-10", "2015-000");
  //password
  await page.click(".v-input #password");
  await page.type(".v-input #password", "newpassword");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Validation for changed password
//admin can changed password
test("Validation for Admin can changed new password", async () => {
  //dashboard
  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content > p"
  );
  //settings
  await page.waitForSelector(
    ".dropdown > .md-button > .md-ripple > .md-button-content > .md-icon"
  );
  await page.click(
    ".dropdown > .md-button > .md-ripple > .md-button-content > .md-icon"
  );
  //change password
  await page.waitForSelector(
    ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
  );
  await page.click(
    ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(1) > a"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//validation for admin can manage user accounts
//admin can view user accounts
test("Validation for Admin view user accounts", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/dashboard");
  await page.setViewport({ width: 1366, height: 768 });
  //user account dashboard
  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content > p"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content > p"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//admin can add user accounts
test("Validation for Admin can add user accounts", async () => {
  let input__slotsrdesc;
  await page.waitForSelector(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );
  await page.click(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );

  input__slotsrdesc = await page.$("#input-87");
  await input__slotsrdesc.click({ clickCount: 1 });
  await input__slotsrdesc.type("Registrar");
  await page.keyboard.press("Enter");
  //Save button
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//admin can search user accounts
test("Validation for Admin can search user accounts", async () => {
  let input__slotsuser;
  //search bar
  await page.click(".v-input #input-46");
  await page.type(".v-input #input-46", "2019-0100");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-46", "2019-0100")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//admin can reset password
test("Validation for Admin can reset password", async () => {
  //reset password button
  await page.waitForSelector(
    "tbody > tr > .text-start > .default > .v-btn__content"
  );
  await page.click("tbody > tr > .text-start > .default > .v-btn__content");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      "tbody > tr > .text-start > .default > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Manage Courses
//Admin View All Courses
test("Validation for Admin can view All Courses", async () => {
  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content > p"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list > .md-list-item:nth-child(3) > .md-list-item-router > .md-list-item-content > p"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//admin can add Courses
xtest("Validation for Admin can add Courses", async () => {
  //add button
  await page.waitForSelector(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );
  await page.click(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );
  //course code
  await page.click(".v-input #input-77");
  await page.type(".v-input #input-77", "BSCPE");
  //course description
  await page.click(".v-input #input-80");
  await page.type(".v-input #input-80", "BS in Computer Engineering");
  //status
  await page.waitForSelector(
    ".v-input > .v-input__control > .v-input__slot > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );
  await page.click(
    ".v-input > .v-input__control > .v-input__slot > .v-input--selection-controls__input > .v-input--selection-controls__ripple"
  );
  //save
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Admin can Search Courses
xtest("Validation for Admin can search Courses", async () => {
  //search
  await page.click(".v-input #input-46");
  await page.type(".v-input #input-46", "BSCPE");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-46", "BSCPE")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Admin can Update Course details
xtest("Validation for Admin can Update Course Details", async () => {
  await page.waitForSelector(
    "tbody > tr > .text-start > .mb-2 > .v-btn__content"
  );
  await page.click("tbody > tr > .text-start > .mb-2 > .v-btn__content");

  //Course Code
  //clear fields
  await page.focus(".v-input #input-77");
  await page.keyboard.down("Control");
  await page.keyboard.press("A");
  await page.keyboard.up("Control");
  await page.keyboard.press("Backspace");
  await page.keyboard.type("BSCOE");

  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Manage Subjects
//Admin view all subjects
test("Validation for Admin view all subject", async () => {
  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content > p"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list > .md-list-item:nth-child(4) > .md-list-item-router > .md-list-item-content > p"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//admin can add subjects
xtest("Validation for Admin can add subject", async () => {
  await page.waitForSelector(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );
  await page.click(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );

  await page.click(".v-input #input-77");
  await page.type(".v-input #input-77", "test");

  await page.click(".v-input #input-80");
  await page.type(".v-input #input-80", "test");

  await page.click(".v-input #input-83");
  await page.type(".v-input #input-83", "2");

  //save button
  await page.waitForSelector(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );
  await page.click(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//admin can search subject
xtest("Validation for Admin can search subject", async () => {
  //search bar
  await page.click(".v-input #input-46");
  await page.type(".v-input #input-46", "test");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-46", "test")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//admin can update subject details
xtest("Validation for Admin can update subject details", async () => {
  await page.waitForSelector(
    "tbody > tr > .text-start > .mb-2 > .v-btn__content"
  );
  await page.click("tbody > tr > .text-start > .mb-2 > .v-btn__content");

  //subject code
  await page.focus(".v-input #input-77");
  await page.keyboard.down("Control");
  await page.keyboard.press("A");
  await page.keyboard.up("Control");
  await page.keyboard.press("Backspace");
  await page.keyboard.type("TECN");

  //subject desc
  await page.focus(".v-input #input-80");
  await page.keyboard.down("Control");
  await page.keyboard.press("A");
  await page.keyboard.up("Control");
  await page.keyboard.press("Backspace");
  await page.keyboard.type("Technology");

  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Manage Sections
//Admin can view Sections
test("Validation for Admin can view Sections", async () => {
  await page.waitForSelector(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(5) > .md-list-item-router > .md-list-item-content"
  );
  await page.click(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(5) > .md-list-item-router > .md-list-item-content"
  );
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".sidebar-wrapper > .md-list > .md-list-item:nth-child(5) > .md-list-item-router > .md-list-item-content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Admin can add Sections
xtest("Validation for Admin can add Sections", async () => {
  //add
  await page.waitForSelector(
    ".v-application--wrap > .v-data-table > .v-sheet > .v-toolbar__content > .mb-2"
  );
  await page.click(
    ".v-application--wrap > .v-data-table > .v-sheet > .v-toolbar__content > .mb-2"
  );
  //section id
  await page.click(".v-input #input-77");
  await page.type(".v-input #input-77", "B");
  //subject code
  input__slotsrdesc = await page.$("#input-80");
  await input__slotsrdesc.click({ clickCount: 1 });
  await input__slotsrdesc.type("TECN");
  await page.keyboard.press("Enter");
  //class size
  await page.click(".v-input #input-87");
  await page.type(".v-input #input-87", "48");
  //save button
  await page.waitForSelector(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );
  await page.click(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//admin can search section
xtest("Validation for Admin can search Sections", async () => {
  await page.click(".v-input #input-46");
  await page.type(".v-input #input-46", "test");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-46", "TECN")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//admin can update section
xtest("Validation for Admin can update Sections", async () => {
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > .text-start > .mb-2 > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(1) > .text-start > .mb-2 > .v-btn__content"
  );

  //class size
  await page.focus(".v-input #input-87");
  await page.keyboard.down("Control");
  await page.keyboard.press("A");
  await page.keyboard.up("Control");
  await page.keyboard.press("Backspace");
  await page.keyboard.type("50");

  //save
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Manage School Year
//Admin View School year
test("Validation for Admin View School Year", async () => {
  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(6) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(6) > .md-list-item-router > .md-list-item-content > p"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list > .md-list-item:nth-child(6) > .md-list-item-router > .md-list-item-content > p"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Admin Search School Year
xtest("Validation for Admin Search School Year", async () => {
  await page.click(".v-input #input-46");
  await page.type(".v-input #input-46", "2019-2020");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-46", "2019-2020")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Admin Add School Year
xtest("Validation for Admin Add School Year", async () => {
  await page.waitForSelector(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );
  await page.click(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-2 > .v-btn__content"
  );

  await page.click(".v-input #input-67");
  await page.type(".v-input #input-67", "2026-2027");

  await page.click(".v-input #input-70");
  await page.type(".v-input #input-70", "2");

  await page.waitForSelector(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );
  await page.click(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//manage student information
//View all student information
test("Validation for Admin view all Student information", async () => {
  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(7) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(7) > .md-list-item-router > .md-list-item-content > p"
  );
  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list > .md-list-item:nth-child(7) > .md-list-item-router > .md-list-item-content > p"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Add new student
xtest("Validation for Admin Add new Student", async () => {
  await page.waitForSelector(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(7) > .md-list-item-router > .md-list-item-content"
  );
  await page.click(
    ".sidebar-wrapper > .md-list > .md-list-item:nth-child(7) > .md-list-item-router > .md-list-item-content"
  );

  //add
  await page.waitForSelector(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-1 > .v-btn__content"
  );
  await page.click(
    ".v-data-table > .v-sheet > .v-toolbar__content > .mb-1 > .v-btn__content"
  );

  //given
  await page.click(".v-input #input-80");
  await page.type(".v-input #input-80", "Jammel");

  await page.click(".v-input #input-83");
  await page.type(".v-input #input-83", "Milan");
  //date
  await page.waitForSelector(".v-input #input-87");
  await page.click(".v-input #input-87");
  await page.waitForSelector(
    "tbody > tr > td > .v-btn--outlined > .v-btn__content"
  );
  await page.click("tbody > tr > td > .v-btn--outlined > .v-btn__content");
  //year level
  await page.click(".v-input #input-92");
  await page.type(".v-input #input-92", "1");
  //course code
  await page.click(".v-input #input-95");
  await page.type(".v-input #input-95", "BSCPE");

  //save
  await page.waitForSelector(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );
  await page.click(
    ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3) > .v-btn__content"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//Admin can search student
xtest("Validation for Admin search Student", async () => {
  //search
  await page.click(".v-input #input-46");
  await page.type(".v-input #input-46", "Juan");

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(".v-input #input-46", "clay")
      .innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 2000000);

//update student details
xtest("Validation for Admin update Student information", async () => {
  await page.waitForSelector(
    "tbody > tr:nth-child(1) > .text-start > .mb-2 > .v-btn__content"
  );
  await page.click(
    "tbody > tr:nth-child(1) > .text-start > .mb-2 > .v-btn__content"
  );
  //given name
  await page.focus(".v-input #input-80");
  await page.keyboard.down("Control");
  await page.keyboard.press("A");
  await page.keyboard.up("Control");
  await page.keyboard.press("Backspace");
  await page.keyboard.type("JunJun");
  //Save button
  await page.waitForSelector(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );
  await page.click(
    ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".v-dialog__content > .v-dialog > .v-card > .v-card__actions > .v-btn:nth-child(3)"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
  await browser.close();
}, 2000000);
