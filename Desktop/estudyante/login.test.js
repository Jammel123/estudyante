const puppeteer = require("puppeteer");
let page;
let browser;

test("Invalid Username and valid Password", async () => {
  browser = await puppeteer.launch({
    headless: true,
    slowMo: 30,
    args: ["--proxy-server= 172.16.1.6:3128"]
  });
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });

  //username
  await page.click(".v-input #input-10");
  await page.type(".v-input #input-10", "test");

  await page.click(".v-input #password");
  await page.type(".v-input #password", "newpassword");

  //sign in button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Valid Username and Invalid Password
test("Valid Username and Invalid Password", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });

  //username
  await page.click(".v-input #input-10");
  await page.type(".v-input #input-10", "2015-000");
  //password
  await page.click(".v-input #password");
  await page.type(".v-input #password", "testpassword");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Invalid username and Invalid Password
test("Invalid Username and Invalid Password", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });

  //username
  await page.click(".v-input #input-10");
  await page.type(".v-input #input-10", "test");

  await page.click(".v-input #password");
  await page.type(".v-input #password", "testpassword");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });

  expect(data).toBeDefined();
}, 200000);

//Null username and Null Password
test("Null Username and Null Password", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });

  //username
  await page.waitForSelector(".v-input #input-10");
  await page.click(".v-input #input-10");

  await page.waitForSelector(".v-input #password");
  await page.click(".v-input #password");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Null username and valid Password
test("Null Username and Valid Password", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });
  //username
  await page.waitForSelector(".v-input #input-10");
  await page.click(".v-input #input-10");

  await page.click(".v-input #password");
  await page.type(".v-input #password", "newpassword");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Valid username and Null Password
test("Valid Username and Null Password", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });

  //username
  await page.click(".v-input #input-10");
  await page.type(".v-input #input-10", "2015-000");

  await page.waitForSelector(".v-input #password");
  await page.click(".v-input #password");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
}, 200000);

//Valid Username and Password
test("Valid Username and Valid Password", async () => {
  page = await browser.newPage();
  await page.goto("http://docker.jltechsol.com/#/signin");
  await page.setViewport({ width: 1366, height: 768 });
  //username
  await page.click(".v-input #input-10");
  await page.type(".v-input #input-10", "2015-000");

  await page.click(".v-input #password");
  await page.type(".v-input #password", "newpassword");
  //save button
  await page.waitForSelector(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );
  await page.click(
    ".row > .col-sm-8 > .elevation-12 > .v-card__actions > .v-btn"
  );

  await page.waitForSelector(
    ".md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content > p"
  );
  await page.click(
    ".md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content > p"
  );

  const data = await page.evaluate(() => {
    let Invalid = document.querySelector(
      ".md-list > .md-list-item:nth-child(2) > .md-list-item-router > .md-list-item-content > p"
    ).innerText;
    return Invalid;
  });
  expect(data).toBeDefined();
  //setting option
  await page.waitForSelector(
    ".dropdown > .md-button > .md-ripple > .md-button-content > .md-icon"
  );
  await page.click(
    ".dropdown > .md-button > .md-ripple > .md-button-content > .md-icon"
  );
//logout
  await page.waitForSelector(
    ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(2) > a"
  );
  await page.click(
    ".md-list-item-content > .dropdown > .dropdown-menu > li:nth-child(2) > a"
  );
  await browser.close();
}, 2000000);
